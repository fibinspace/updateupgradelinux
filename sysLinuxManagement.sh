#!/bin/bash

clear

## main menu func - read, case, while loop
menu () { while true
do

tput setaf 10;
echo ""
echo " ~:Update && Upgrade script:~"
echo ""
echo " 1: Debian apt update"
echo " 2: Debian apt upgrade"
echo " 3: Show me the groups"
echo " 4: Manjaro/Arch"
echo " 5: CentOS 7"
echo " X: X or x to Exit/Quit"
echo ""
echo " ~:~:~:~:~:~:~:~:~:~:~:~:~:~:~"
echo ""
read -p " Please enter the number: " number
case $number in
    1) aptUpdate;;
    2) aptUpgrade;;
    3) showMeMyGroups;;
    4) archi;;
    5) centos;;
    x|X) exit;;

esac
done
} 

##apt update debian
aptUpdate() {
    tput setaf 3;
    sudo apt update;
}

##apt upgrade debian
aptUpgrade() {
    tput setaf 12; sudo apt upgrade; tput sgro
    
}

##groups
showMeMyGroups() {
    echo " "
    tput setaf 1; groups; tput sgr0


}

##archi
archi() {
    sudo pacman -Syy && sudo aptitude update
    clear
}

## centOS 7
centos() {
    yum update
    clear
}
## needed to run menu func
menu
